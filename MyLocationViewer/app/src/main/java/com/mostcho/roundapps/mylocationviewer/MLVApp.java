package com.mostcho.roundapps.mylocationviewer;

import android.app.Application;

import com.mostcho.roundapps.mylocationviewer.utils.AppPrefs;

public class MLVApp extends Application {
    // ---------------------------------------------------------------------------------------------
    // Application lifecycle
    // ---------------------------------------------------------------------------------------------
    @Override
    public void onCreate() {
        super.onCreate();
        AppPrefs.init(this);
    }
}

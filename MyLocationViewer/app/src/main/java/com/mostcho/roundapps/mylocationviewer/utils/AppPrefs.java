package com.mostcho.roundapps.mylocationviewer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppPrefs {
    // ---------------------------------------------------------------------------------------------
    // Constants
    // ---------------------------------------------------------------------------------------------
    private static final String PREF_KEY_LAST_UPDATE_TIEM = "pref_key_last_update_time";

    // ---------------------------------------------------------------------------------------------
    // Fields
    // ---------------------------------------------------------------------------------------------
    private static SharedPreferences sPrefs;

    // ---------------------------------------------------------------------------------------------
    // Init methods
    // ---------------------------------------------------------------------------------------------
    public static void init(Context context) {
        sPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    // ---------------------------------------------------------------------------------------------
    // Public methods
    // ---------------------------------------------------------------------------------------------
    public static void writeLastUpdateTime(String lastUpdateTime) {
        sPrefs.edit()
                .putString(PREF_KEY_LAST_UPDATE_TIEM, lastUpdateTime)
                .apply();
    }

    public static String readLastUpdateTime() {
        return sPrefs.getString(PREF_KEY_LAST_UPDATE_TIEM, null);
    }
}

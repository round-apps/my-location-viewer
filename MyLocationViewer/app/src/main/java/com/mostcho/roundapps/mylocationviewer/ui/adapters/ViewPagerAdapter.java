package com.mostcho.roundapps.mylocationviewer.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mostcho.roundapps.mylocationviewer.ui.fragments.CoordinatesFragment;
import com.mostcho.roundapps.mylocationviewer.ui.fragments.GMapFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public static final int TAB_COORDINATES = 0;
    public static final int TAB_GMAP = 1;


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        switch (pos) {
            case TAB_COORDINATES:
                return CoordinatesFragment.newInstance();
            case TAB_GMAP:
                return GMapFragment.newInstance();
            default:
                return CoordinatesFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
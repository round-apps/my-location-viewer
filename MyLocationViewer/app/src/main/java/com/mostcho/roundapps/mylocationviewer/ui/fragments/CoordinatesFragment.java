package com.mostcho.roundapps.mylocationviewer.ui.fragments;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mostcho.roundapps.mylocationviewer.R;
import com.mostcho.roundapps.mylocationviewer.interfaces.IOnFragmentVisibilityListener;

public class CoordinatesFragment extends BaseFragment {
    // -----------------------------------------------------------------------------------------------------------------
    // Constants
    // -----------------------------------------------------------------------------------------------------------------
    private static final String TAG = CoordinatesFragment.class.getSimpleName();

    // -----------------------------------------------------------------------------------------------------------------
    // Fields
    // -----------------------------------------------------------------------------------------------------------------
    private boolean isVisible = false;
    private IOnFragmentVisibilityListener onFragmentVisibilityListener;
    private TextView tvLastUpdateTime;
    private TextView tvLatitude;
    private TextView tvLongitude;
    private TextView tvAltitude;

    // -----------------------------------------------------------------------------------------------------------------
    // Instance creation
    // -----------------------------------------------------------------------------------------------------------------
    public CoordinatesFragment() {
    }

    public static CoordinatesFragment newInstance() {
        return new CoordinatesFragment();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Fragment lifecycle
    // -----------------------------------------------------------------------------------------------------------------
    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        if (activity instanceof IOnFragmentVisibilityListener) {
            onFragmentVisibilityListener = (IOnFragmentVisibilityListener) activity;
        } else {
            throw new RuntimeException(activity.toString() + " must implement " + IOnFragmentVisibilityListener.class.getSimpleName());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_coordinates, container, false);
        tvLastUpdateTime = (TextView) rootView.findViewById(R.id.tv_last_update_time);
        tvLatitude = (TextView) rootView.findViewById(R.id.tv_lat);
        tvLongitude = (TextView) rootView.findViewById(R.id.tv_lng);
        tvAltitude = (TextView) rootView.findViewById(R.id.tv_alt);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible && onFragmentVisibilityListener != null) {
            onFragmentVisibilityListener.onFragmentVisible(this);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Public methods
    // -----------------------------------------------------------------------------------------------------------------
    public void updateData(Location location, String lastUpdateTime) {
        Log.d(TAG, location.toString() + " from " + lastUpdateTime);
        if (lastUpdateTime != null) {
            tvLastUpdateTime.setText(lastUpdateTime);
        }
        tvLatitude.setText(getString(R.string.latitude, location.getLatitude()));
        tvLongitude.setText(getString(R.string.longitude, location.getLongitude()));
        tvAltitude.setText(getString(R.string.altitude, location.getAltitude()));
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Override methods
    // -----------------------------------------------------------------------------------------------------------------
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            isVisible = true;
            if (onFragmentVisibilityListener != null) {
                onFragmentVisibilityListener.onFragmentVisible(this);
            }
        }
    }
}

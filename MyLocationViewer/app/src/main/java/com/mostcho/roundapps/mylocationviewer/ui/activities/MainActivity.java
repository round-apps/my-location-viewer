package com.mostcho.roundapps.mylocationviewer.ui.activities;

import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.mostcho.roundapps.mylocationviewer.R;
import com.mostcho.roundapps.mylocationviewer.interfaces.IOnFragmentVisibilityListener;
import com.mostcho.roundapps.mylocationviewer.ui.adapters.ViewPagerAdapter;
import com.mostcho.roundapps.mylocationviewer.ui.fragments.CoordinatesFragment;
import com.mostcho.roundapps.mylocationviewer.ui.fragments.GMapFragment;
import com.mostcho.roundapps.mylocationviewer.ui.views.VerticalViewPager;
import com.mostcho.roundapps.mylocationviewer.utils.AppPrefs;

import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements IOnFragmentVisibilityListener {
    // -----------------------------------------------------------------------------------------------------------------
    // Constants
    // -----------------------------------------------------------------------------------------------------------------
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1 * 60 * 1000; // 1 min
    //private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 30 * 1000; // 30 sec
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // -----------------------------------------------------------------------------------------------------------------
    // Fields
    // -----------------------------------------------------------------------------------------------------------------
    private Fragment visibleFragment;
    private VerticalViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private Location lastLocation;

    // -----------------------------------------------------------------------------------------------------------------
    // Activity lifecycle
    // -----------------------------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (VerticalViewPager) findViewById(R.id.view_pager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpLocationTracker();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (fusedLocationClient != null && locationCallback != null) {
            fusedLocationClient.removeLocationUpdates(locationCallback);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Private methods
    // -----------------------------------------------------------------------------------------------------------------
    private synchronized void setUpLocationTracker() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Log.d(TAG, "New location: " + locationResult.getLastLocation());
                AppPrefs.writeLastUpdateTime(DateFormat.getTimeInstance().format(new Date()));
                updateLocation(locationResult.getLastLocation(), AppPrefs.readLastUpdateTime());
            }
        };

        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        //Get last known location
        try {
            fusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        Log.d(TAG, "Last known location: " + task.getResult().toString());
                        updateLocation(task.getResult(), null);
                    } else {
                        Log.w(TAG, "Failed to get location.");
                    }
                }
            });
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }

        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();
        settingsClient.checkLocationSettings(locationSettingsRequest)
            .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                    Log.d(TAG, "All location settings are satisfied.");
                    try {
                        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                    } catch (SecurityException unlikely) {
                        Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
                    }
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.e(TAG, "Location settings are unavailable.");
                            break;
                    }
                }
            });
    }

    private void updateLocation(Location location, String lastUpdateTime) {
        lastLocation = location;
        if (visibleFragment != null && visibleFragment.isVisible()) {
            if (visibleFragment instanceof CoordinatesFragment) {
                ((CoordinatesFragment) visibleFragment).updateData(location, lastUpdateTime);
            } else if (visibleFragment instanceof GMapFragment) {
                ((GMapFragment) visibleFragment).updateData(location);
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Override methods
    // -----------------------------------------------------------------------------------------------------------------
    @Override
    public void onBackPressed() {
        if (visibleFragment instanceof GMapFragment) {
            viewPager.setCurrentItem(ViewPagerAdapter.TAB_COORDINATES, true);
            visibleFragment = viewPagerAdapter.getItem(ViewPagerAdapter.TAB_COORDINATES);
        } else {
            super.onBackPressed();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Callback methods
    // -----------------------------------------------------------------------------------------------------------------
    @Override
    public void onFragmentVisible(Fragment fragment) {
        visibleFragment = fragment;
        if (visibleFragment instanceof GMapFragment) {
            if (lastLocation != null) {
                ((GMapFragment) visibleFragment).updateData(lastLocation);
            }
        }
    }
}

package com.mostcho.roundapps.mylocationviewer.interfaces;


import android.support.v4.app.Fragment;

public interface IOnFragmentVisibilityListener {
    void onFragmentVisible(Fragment fragment);
}

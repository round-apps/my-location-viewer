package com.mostcho.roundapps.mylocationviewer.ui.fragments;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.mostcho.roundapps.mylocationviewer.R;
import com.mostcho.roundapps.mylocationviewer.interfaces.IOnFragmentVisibilityListener;

public class GMapFragment extends BaseFragment implements OnMapReadyCallback {
    // -----------------------------------------------------------------------------------------------------------------
    // Constants
    // -----------------------------------------------------------------------------------------------------------------
    private static final float MAP_ZOOM_LEVEL = 14.5f;

    // -----------------------------------------------------------------------------------------------------------------
    // Fields
    // -----------------------------------------------------------------------------------------------------------------
    private boolean isVisible = false;
    private GoogleMap map; // Might be null if Google Play services APK is not available.
    private IOnFragmentVisibilityListener onFragmentVisibilityListener;

    // -----------------------------------------------------------------------------------------------------------------
    // Instance creation
    // -----------------------------------------------------------------------------------------------------------------
    public GMapFragment() {
    }

    public static GMapFragment newInstance() {
        return new GMapFragment();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Fragment lifecycle
    // -----------------------------------------------------------------------------------------------------------------
    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        if (activity instanceof IOnFragmentVisibilityListener) {
            onFragmentVisibilityListener = (IOnFragmentVisibilityListener) activity;
        } else {
            throw new RuntimeException(activity.toString() + " must implement " + IOnFragmentVisibilityListener.class.getSimpleName());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gmap, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        if (isVisible && onFragmentVisibilityListener != null) {
            onFragmentVisibilityListener.onFragmentVisible(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (map != null) {
            map.clear();
            map = null;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Public methods
    // -----------------------------------------------------------------------------------------------------------------
    public void updateData(Location location) {
        if (map != null) {
            /*LatLng mPosition = new LatLng(location.getLatitude(),
                    location.getLongitude());*/
            CameraUpdate cameraUpdate =
                CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                    location.getLongitude()), MAP_ZOOM_LEVEL);
            map.animateCamera(cameraUpdate);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Private methods
    // -----------------------------------------------------------------------------------------------------------------
    private void setUpMapIfNeeded() {
        if (map == null) {
            SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            if (fragment != null) {
                fragment.getMapAsync(this);
            }
        } else {
            setUpMap();
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera.
     * <p/>
     * This should only be called once and when we are sure that {@link #map} is not null.
     */
    private void setUpMap() {
        map.setMyLocationEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setZoomGesturesEnabled(true);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Override methods
    // -----------------------------------------------------------------------------------------------------------------
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            isVisible = true;
            if (onFragmentVisibilityListener != null) {
                onFragmentVisibilityListener.onFragmentVisible(this);
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Callback methods
    // -----------------------------------------------------------------------------------------------------------------
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (map != null) {
            setUpMap();
        }
    }
}
